# Android Automation Bootcamp
***
## Dependencies
Dependencies yang terdapat pada bootcamp android adalah
1. Java 17 -> Sebagai programming language standar yang dipakai untuk membuat program automation
2. Appium 2 -> Sebagai server yang digunakan agar dapat menjalankan automation
3. Gradle 8.3.0 -> Gradle adalah sebuah tools build automation yang berfungsi untuk mengoptimasi proses kompilasi, pengujian, dan deployment
4. Cucumber -> Cucumber adalah sebuah behavior-driven-development yang berfungsi untuk merubah human language menjadi machine language agar automation dapat berjalan
5. UiAutomator2 Driver -> Salah satu driver otomatisasi yang didukung oleh Appium untuk mengotomatisasi aplikasi Android. UiAutomator2 Driver menggunakan kerangka kerja UI Automator untuk mengontrol dan menguji aplikasi Android.
6. Appium Inspector -> Digunakan untuk menemukan element-element pada sebuah aplikasi agar dapat membuat program automation
7. Android Studio -> Digunakan sebagai Device Virtual untuk memastikan program automation berjalan dengan lancar

***
## Setup Automation
1. Download JDK 17 [disini](https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html), kemudian install hinggal selesai
2. Add path java/jdk ke environment variable
3. Download git [disini](https://git-scm.com/downloads) dan pilih sesuai dengan OS dan install hingga selesai
4. Install appium dengan cara:
    1. Buka git bash dan ketik npm i --location=global appium dan enter
    2. Tunggu hingga proses instalasi selesai
5. Input "appium driver install uiautomator2" pada git bash dan klik enter, tunggu hingga instalasi selesai
6. Download Appium inspector [disini](https://github.com/appium/appium-inspector/releases)
7. Download Android Studio [disini](https://developer.android.com/studio) dan install
8. Setting android emulator berdasarkan link [ini](https://developer.android.com/design-for-safety/privacy-sandbox/download) (Disarankan untuk menggunakan OS Android 12 agar tidak terlalu berat)
9. Add path android SDK ke PATH environment variable
10. Pada git bash ketik "appium" untuk menyalakan server appium
11. Anda dapat melanjutkan untuk membuat program automation